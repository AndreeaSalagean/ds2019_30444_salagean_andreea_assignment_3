package controller;

public interface CallbackInterface {
    void onButtonClicked(String name);
}
