package controller;

import com.grpc.helloworld.*;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import model.Medication;
import org.springframework.stereotype.Controller;
import presentation.View;



import javax.swing.*;
import javax.swing.Timer;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.List;


@Controller
public class ViewController implements CallbackInterface {
    private View view;
    private List<Medication> medicationList;
    private  MedTakenGrpc.MedTakenBlockingStub stub;
    private MedPlanSErviceGrpc.MedPlanSErviceBlockingStub planStub;
    private Timer timer;
    private Boolean[] validate;
    private Boolean[] validateNotTaken ;
    private Date current_date = new GregorianCalendar(2019, Calendar.FEBRUARY, 11, 9 , 20, 0).getTime();
    private Date max = new GregorianCalendar(2019, Calendar.FEBRUARY, 11, 14, 0, 0).getTime();

    public ViewController(){

        initialize();
        this.medicationList = getMedPlan();
        this.view = new View(medicationList);
        this.view.setListener(this);
       // setCurrent_date();
        startTimer();
        initBoolean();
        initValidate();
    }


    public void initialize(){

        ManagedChannel channel = ManagedChannelBuilder.forAddress("localhost", 6565)
                .usePlaintext()
                .build();

        this.planStub =
                MedPlanSErviceGrpc.newBlockingStub(channel);


        this.stub =
                MedTakenGrpc.newBlockingStub(channel);

    }

    public List<Medication> getMedPlan(){
        MedicationResponse medicalPlans = planStub.getMedication(MedQuery.newBuilder()
                .setId("7")
                .build());
        this.medicationList = new ArrayList<>();

        for(com.grpc.helloworld.Medication m: medicalPlans.getMedicationsList()){
            medicationList.add(new model.Medication(Integer.parseInt(m.getId()), m.getName(), m.getDosage(), m.getStart(), m.getEnd()));
        }
        return this.medicationList;

    }

    public void startTimer(){

        timer = new Timer(10, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                String currentDate = current_date.toString();
                if (current_date.getTime()== max.getTime()){
                    medicationList = getMedPlan();
                    view.refreshTable(medicationList);
                    initBoolean();
                    initValidate();
                    setCurrent_date();

                }

                view.getTimerLabel().setText(currentDate.replace("EET", ""));
                for(Medication m : medicationList) {
                    if(m.getStar_time().getTime() < current_date.getTime() && m.getEnd_time().getTime() > current_date.getTime() ){

                        if(validate[getIndex(m.getId())].booleanValue() == false){
                            //validateNotTaken[getIndex(m.getId())] = true;
                            view.getButton(m.getName()).setEnabled(true);
                        }
                        else {
                            view.getButton(m.getName()).setEnabled(false);
                            validateNotTaken[getIndex(m.getId())] = false;
                        }

                    }else{

                        view.getButton(m.getName()).setEnabled(false);
                        if(m.getEnd_time().getTime() < current_date.getTime() && validateNotTaken[getIndex(m.getId())]){
                            sendNotTaken(m.getName());
                            validateNotTaken[getIndex(m.getId())] = false;
                        }
                    }
                }

                    current_date.setTime(current_date.getTime() + 1000);

            }
        });
        timer.start();

    }


    @Override
    public void onButtonClicked(String name) {
        String mess[] = name.split(" ");
        Integer id =null;

        Medication m = null;
        List<Medication> med = medicationList;

        for(Medication medication: medicationList){
            if(medication.getName().equals(mess[1])){
                id = medication.getId();
            }
        }

        ServerResponseTaken medication = stub.setTaken(MedNameTaken.newBuilder()
                .setName("Taken Medication " + mess[1] )
                .setMedId(id.toString())
                .build());

        deleteFromList(mess[1]);
        validate[getIndexFromName(mess[1])] = true;

    }


    public void sendNotTaken(String name){
        Integer id =null;
        List<Medication> med = medicationList;

        for(Medication medication: medicationList){
            if(medication.getName().equals(name)){
                id = medication.getId();
            }
        }

        ServerResponseTaken medication = stub.setTaken(MedNameTaken.newBuilder()
                .setName("NOT Taken Medication: " + name )
                .setMedId(id.toString())
                .build());
    }



    public void deleteFromList(String name){
        JTable jmodel = view.getTable();
        DefaultTableModel model = (DefaultTableModel)jmodel.getModel();
        for (int i = 0; i < model.getRowCount(); i++) {
            if (((String)model.getValueAt(i, 0)).equals(name)) {
                model.removeRow(i);
                break;
            }
        }
    }

    public void initBoolean(){

         validate = view.getValidate();

    }

    public void initValidate(){
        validateNotTaken = new Boolean[this.medicationList.size()];

        for(int i =0; i<this.medicationList.size();i ++){
            validateNotTaken[i] = true;
        }
    }

    public Integer getIndex(int id){
        List<Medication> med = medicationList;
        for(int i =0; i< med.size();i++){
            if(med.get(i).getId().equals(id)){
                return i;
            }
        }
        return 0;
    }

    public Integer getIndexFromName(String bName){
        Medication m = null;
        List<Medication> med = medicationList;

        for(Medication medication: medicationList){
            if(medication.getName().equals(bName)){
                m = medication;
            }
        }

        for(int i =0 ; i< medicationList.size();i++){
            if(med.get(i).getId().equals(m.getId())){
                return i;
            }
        }
        return 0;
    }


    public Date getCurrent_date() {
        return current_date;
    }

    public void setCurrent_date() {

        Date d = this.medicationList.get(0).getStar_time();
        System.out.println(d);

        Calendar cal = Calendar.getInstance();

        cal.setTime(d);
        cal.add(Calendar.HOUR, -1);
        cal.add(Calendar.MINUTE, 30);

        this.current_date = cal.getTime();



    }
}
