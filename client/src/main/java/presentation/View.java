package presentation;

import controller.CallbackInterface;
import model.Medication;
import org.springframework.stereotype.Component;

import javax.security.auth.callback.Callback;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;


public class View extends JFrame {

    private List<Medication> medicationList ;
    private JPanel panel;
    private JPanel fPanel;
    private JLabel timerLabel;
    private JTable table;
    private JPanel panelTake;
    private JButton[] buttonsMed;
    private  String[] OPTIONS;
    private Boolean[] validate;
    private CallbackInterface callbackInterface;
    private String[] columns = {"Medication Name","Start Interval","End Interval","Dosage"};

    public  View(List<Medication> medications){
        this.medicationList = medications;
        panel = new JPanel();
        panel.setLayout(null);

        fPanel = new JPanel();
        fPanel.setLayout(null);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setSize(800, 600);
        this.setTitle("Medication Plan");
        this.setVisible(true);
        //String[] columns = {"Medication Name","Start Interval","End Interval","Dosage"};
        String[][] data = display_medication(medicationList);
        TableModel tableModel = new DefaultTableModel(data, columns);

        table = new JTable(tableModel);
        table.setBounds(500,150,600,400);
        table.setBackground(Color.LIGHT_GRAY);
        table.setForeground(Color.black);
        Font font = new Font("",1,22);
        Font font2 = new Font("",2,18);
        table.setFont(font);
        table.setRowHeight(30);


        JLabel label = new JLabel("This is your medication plan!");
        label.setFont(font);
        label.setBounds(200, 40, 600, 25);

        JLabel label2 = new JLabel("Current Time");
        label2.setFont(font2);
        label2.setBounds(150, 250, 750, 25);

        JScrollPane pane = new JScrollPane(table);
        pane.setBounds(25, 80, 650, 150);
        timerLabel = new JLabel();
        timerLabel.setBounds(290, 250, 600,25);

        panelTake = new JPanel();
        panelTake.setBounds(200,380, 600, 200);
        panel.setBounds(10, 20, 700, 350);
        panelTake.setLayout(new BoxLayout(panelTake, BoxLayout.Y_AXIS));
        OPTIONS = new String[20];
        validate = new Boolean[medications.size()];

        for(int i = 0; i < medications.size(); i++){
            OPTIONS[i] = medications.get(i).getName();

        }

        buttonsMed = new JButton[OPTIONS.length];
        for (int i = 0; i < medicationList.size(); i++) {
            buttonsMed[i] = new JButton("Take " + OPTIONS[i]);
            validate[i] = false;
            panelTake.add(buttonsMed[i]);
            panelTake.add(Box.createVerticalStrut(15));
            buttonsMed[i].setEnabled(false);
        }


        panel.add(timerLabel);
        panel.add(label);
        panel.add(label2);
        panel.add(pane);

        fPanel.add(panel);
        fPanel.add(panelTake);

        this.add(fPanel);

        setListenersOnButtons();
    }


    public String[][] display_medication(List<Medication> meds) {
        String[][] med=new String[20][20];
        List<Medication> list= meds;
        System.out.println(medicationList.get(1).getName());
        Object[] row= new Object[3];
        for(int i=0;i<list.size();i++) {
            med[i][0]=list.get(i).getName();
            med[i][1]=list.get(i).getStar_time().toString();
            med[i][2]=list.get(i).getEnd_time().toString();
            med[i][3]=list.get(i).getDosage();

        }
        return med;
    }

    public void refreshTable(List<Medication> medications){

        int rows = medications.size();
        String[][] data = display_medication(medications);

        for(int i = 0; i < rows + 1; ++i)
        {
            for(int j = 0; j < columns.length; ++j)
                table.getModel().setValueAt(data[i][j], i, j);
        }


    }
    public JLabel getTimerLabel() {
        return timerLabel;
    }

    public void setTimerLabel(JLabel timerLabel) {
        this.timerLabel = timerLabel;
    }

    public List<Medication> getMedicationList() {
        return medicationList;
    }

    public void setMedicationList(List<Medication> medicationList) {
        this.medicationList = medicationList;
}

    public String[] getOPTIONS() {
        return OPTIONS;
    }

    public void setOPTIONS(String[] OPTIONS) {
        List<Medication> medications = medicationList;
        for(int i = 0; i < medications.size(); i++){
            OPTIONS[i] += medications.get(i).getName();
        }
        this.OPTIONS = OPTIONS;
    }

    public JButton[] getButtonsMed() {
        return buttonsMed;
    }

    public void setButtonsMed(JButton[] buttonsMed) {
        this.buttonsMed = buttonsMed;
    }
    public JButton getButton(String name){
        String name_to_compare = "Take " + name ;
        JButton[] buttons = getButtonsMed();
        for(JButton b : buttons){
            if(b!= null) {
                if (b.getText().equals(name_to_compare)) {
                    //b.setEnabled(true);
                    return b;
                }
            }

        }
        return null;
    }

    public void setListenersOnButtons() {
        for(JButton b: buttonsMed) {
            if(b == null) continue;
            b.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    callbackInterface.onButtonClicked(b.getText());
                }
            });
        }
    }

    public void setListener(CallbackInterface callbackInterface) {
        this.callbackInterface = callbackInterface;
    }

    public JTable getTable() {
        return table;
    }

    public void setTable(JTable table) {
        this.table = table;
    }

    public Boolean[] getValidate() {
        return validate;
    }

    public void setValidate(Boolean[] validate) {
        this.validate = validate;
    }
}
