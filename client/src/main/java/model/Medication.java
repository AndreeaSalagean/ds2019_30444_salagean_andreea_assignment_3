package model;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Medication {
     private Integer id;
     private String name;
     private String dosage;
     private Date star_time;
     private Date end_time;

     public Medication(Integer id, String name, String dosage, String star_time, String end_time){
         this.id= id;
         this.name = name;
         this.dosage = dosage;
         this.star_time = stringToDate(star_time);
         this.end_time = stringToDate(end_time);
     }
    public  Medication(){

    }
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDosage() {
        return dosage;
    }

    public void setDosage(String dosage) {
        this.dosage = dosage;
    }

    public Date getStar_time() {
        return star_time;
    }

    public void setStar_time(Date star_time) {
        this.star_time = star_time;
    }

    public Date getEnd_time() {
        return end_time;
    }

    public void setEnd_time(Date end_time) {
        this.end_time = end_time;
    }
    public Date stringToDate(String s) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try
        {
            Date date = simpleDateFormat.parse(s);
            return date;
        }
        catch (ParseException ex)
        {
            System.out.println("Exception "+ex);
        }
        return null;

    }
}
