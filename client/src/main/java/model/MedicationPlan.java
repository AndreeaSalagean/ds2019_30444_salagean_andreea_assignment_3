package model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MedicationPlan {

    private Integer id;
    private Date start_time;
    private Date end_time ;
    private List<Medication> medicationList;

    public MedicationPlan(Integer id, Date start_time, Date end_time){
        this.id=id;
        this.start_time = start_time;
        this.end_time = end_time;
        this.medicationList = new ArrayList<Medication>();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getStart_time() {
        return start_time;
    }

    public void setStart_time(Date start_time) {
        this.start_time = start_time;
    }

    public Date getEnd_time() {
        return end_time;
    }

    public void setEnd_time(Date end_time) {
        this.end_time = end_time;
    }

    public List<Medication> getMedicationList() {
        return medicationList;
    }

    public void setMedicationList(List<Medication> medicationList) {
        this.medicationList = medicationList;
    }
}
