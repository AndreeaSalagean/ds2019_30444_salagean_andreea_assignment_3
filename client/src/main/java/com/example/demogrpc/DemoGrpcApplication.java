package com.example.demogrpc;

import com.grpc.helloworld.*;
import controller.ViewController;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import presentation.View;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
public class DemoGrpcApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoGrpcApplication.class, args);
		System.setProperty("java.awt.headless", "false");

		SwingUtilities.invokeLater(() -> {
			ViewController viewController = new ViewController();
		});

	}

}
