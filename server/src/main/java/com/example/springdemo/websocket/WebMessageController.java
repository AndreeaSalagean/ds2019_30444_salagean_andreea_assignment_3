package com.example.springdemo.websocket;

import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;
import org.springframework.web.util.HtmlUtils;

@Controller
public class WebMessageController {

    @MessageMapping("/hello")
    @SendTo("/{id}/patients-list")
    public ExchangeMessage sendMessage(ExchangeMessage message) throws Exception {
        return new ExchangeMessage("Hello, " + HtmlUtils.htmlEscape(message.getMessage()) + "!");
    }
}
