package com.example.springdemo.websocket;

public class ExchangeMessage {
    private String message;

    public ExchangeMessage(){}

    public ExchangeMessage(String message){
        this.message = message ;}

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
