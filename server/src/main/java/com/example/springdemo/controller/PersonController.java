package com.example.springdemo.controller;


import com.example.springdemo.dto.PersonDTO;
import com.example.springdemo.dto.PersonViewDTO;
import com.example.springdemo.services.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping(value = "/person")
public class PersonController {

    private final PersonService personService;

    @Autowired
    public PersonController(PersonService personService) {
        this.personService = personService;
    }

    @GetMapping(value = "/{id}")
    public PersonViewDTO findById(@PathVariable("id") Integer id){
        return personService.findUserById(id);
    }

    @GetMapping()
    public List<PersonViewDTO> findAll(){
        return personService.findAll();
    }

    @PostMapping(value = "/authenticate")
    public PersonDTO authenticate(@RequestBody Map<String, String> body){
        System.out.println(body);
        return personService.authenticate(body.get("username"),body.get("password"));
        }


    @PostMapping()
    public Integer insertUserDTO(@RequestBody PersonDTO personDTO){
        return personService.insert(personDTO);
    }

    @PutMapping()
    public Integer updateUser(@RequestBody PersonDTO personDTO) {
        return personService.update(personDTO);
    }

    @DeleteMapping()
    public void delete(@RequestBody PersonViewDTO personViewDTO){
        personService.delete(personViewDTO);
    }
}
