package com.example.springdemo.controller;

import com.example.springdemo.dto.DoctorDTO;
import com.example.springdemo.services.DoctorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = "/doctor")
public class DoctorController {

    private final DoctorService doctorService;

    @Autowired
    public DoctorController(DoctorService doctorService){this.doctorService = doctorService;}

    @GetMapping(value = "/{id}")
    public DoctorDTO findById(@PathVariable("id") Integer id){
        return doctorService.findUserById(id);
    }

    @GetMapping()
    public List<DoctorDTO> findAll(){
        return doctorService.getAll();
    }

    @PostMapping()
    public Integer insertDoctor(@RequestBody DoctorDTO doctorDTO){
        return doctorService.insert(doctorDTO);
    }

    @PutMapping()
    public Integer updateDoctor(@RequestBody DoctorDTO doctorDTO) {
        return doctorService.update(doctorDTO);
    }

    @DeleteMapping()
    public void delete(@RequestBody DoctorDTO doctorDTO){
        doctorService.delete(doctorDTO);
    }
}
