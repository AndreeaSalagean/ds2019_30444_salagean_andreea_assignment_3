package com.example.springdemo.dto.builders;

import com.example.springdemo.dto.PersonDTO;
import com.example.springdemo.entities.Person;

public class PersonBuilder {

    public PersonBuilder() {
    }

    public static PersonDTO generateDTOFromEntity(Person person){
        return new PersonDTO(
                person.getId(),
                person.getEmail(),
                person.getPassword(),
                person.getRole());
    }

    public static Person generateEntityFromDTO(PersonDTO personDTO){
        return new Person(
                personDTO.getId(),
                personDTO.getEmail(),
                personDTO.getPassword(),
                personDTO.getRole());
    }
}
