package com.example.springdemo.dto;

import java.sql.Date;

public class CaregiverDTO {
    private Integer id;
    private String name;
    private String birthDate;
    private String gender;
    private String address;

    public CaregiverDTO(Integer id, String name, String birthDate, String gender, String address){
        this.id = id;
        this.name = name;
        this.birthDate = birthDate;
        this.gender = gender;
        this.address = address;

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBirth_date() {
        return birthDate;
    }

    public void setBirth_date(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
