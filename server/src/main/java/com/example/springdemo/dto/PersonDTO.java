package com.example.springdemo.dto;

import java.util.Objects;

public class PersonDTO {

    private Integer id;
    private String email;
    private String role;
    private String password;

    public PersonDTO() {
    }

    public PersonDTO(Integer id, String email, String password, String role) {
        this.id = id;
        this.email = email;
        this.password = password;
        this.role = role;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }


    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }


//    @Override
//    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (o == null || getClass() != o.getClass()) return false;
//        PersonDTO personDTO = (PersonDTO) o;
//        return Objects.equals(id, personDTO.id) &&
//                Objects.equals(name, personDTO.name) &&
//                Objects.equals(email, personDTO.email);
//    }
//
//    @Override
//    public int hashCode() {
//
//        return Objects.hash(id, name, email);
//    }
}
