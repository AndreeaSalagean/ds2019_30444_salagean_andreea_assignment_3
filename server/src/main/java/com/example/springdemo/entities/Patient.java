package com.example.springdemo.entities;
import javax.persistence.*;

import java.sql.Date;
import java.util.List;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "patient")
public class Patient {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

    @ManyToMany(mappedBy = "patients", fetch = FetchType.LAZY)
    List<Caregiver> caregivers ;

    @OneToMany(mappedBy = "patient")
    private List<MedicationPlan> medicalPlans;

    public List<Message> getMessage_list() {
        return message_list;
    }

    public void setMessage_list(List<Message> message_list) {
        this.message_list = message_list;
    }

    @OneToMany(mappedBy = "patient_id")
    private List<Message> message_list;

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    @OneToOne
    @MapsId
    private Person person;

    @Column(name = "name", length = 100)
    private String name;

    @Column(name = "birthDate")
    private String birthDate;

    @Column(name = "gender")
    private String gender;

    @Column(name = "address")
    private String address;

    @Column(name = "medicalRecord")
    private String medicalRecord;


    public Patient(){

    }

    public Patient(Integer id, String name, String birthDate, String gender, String address, String medicalRecord){
        this.id = id;
        this.name = name;
        this.birthDate = birthDate;
        this.gender = gender;
        this.address = address;
        this.medicalRecord = medicalRecord;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBirth_date() {
        return birthDate;
    }

    public void setBirth_date(String birth_date) {
        this.birthDate = birthDate;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getMedicalRecord() {
        return medicalRecord;
    }

    public void setMedicalRecord(String medicalRecord) {
        this.medicalRecord = medicalRecord;
    }
}
