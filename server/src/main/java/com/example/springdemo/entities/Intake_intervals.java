package com.example.springdemo.entities;

import javax.persistence.*;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "intake_interval")
public class Intake_intervals {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

    @Column(name = "start")
    private String start;

    @Column(name = "end")
    private String end;

    @Column(name="taken")
    private Boolean taken;

    @ManyToOne(cascade = {
            CascadeType.ALL
    }, fetch = FetchType.LAZY)
    @JoinColumn(name = "med_plan_id", nullable = false)
    private MedicationPlan medication_plan;

    @ManyToOne
    @JoinColumn(name = "med_id", nullable = false)
    private Medication medication;

    public Intake_intervals(){}
    public Intake_intervals(Integer id, String start, String end){
        this.id = id;
        this.start = start;
        this.end = end;
        this.taken = false;

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }

    public MedicationPlan getMedication_plan() {
        return medication_plan;
    }

    public void setMedication_plan(MedicationPlan medication_plan) {
        this.medication_plan = medication_plan;
    }

    public Medication getMedication() {
        return medication;
    }

    public void setMedication(Medication medication) {
        this.medication = medication;
    }

    public Boolean getTaken() {
        return taken;
    }

    public void setTaken(Boolean taken) {
        this.taken = taken;
    }
}
