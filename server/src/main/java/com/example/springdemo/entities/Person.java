package com.example.springdemo.entities;

import javax.persistence.*;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "person")
public class Person {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

//    @OneToMany(cascade = CascadeType.ALL)
//    @JoinColumn(name = "doctor_id", referencedColumnName = "id")
//    private Doctor doctor;
    @OneToOne(mappedBy = "person", cascade = CascadeType.ALL)
    private Doctor doctor;

    @OneToOne(mappedBy = "person", cascade = CascadeType.ALL)
    private Caregiver caregiver;

    public void setDoctor(Doctor doctor) {
        this.doctor = doctor;
    }

    public void setCaregiver(Caregiver caregiver) {
        this.caregiver = caregiver;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    @OneToOne(mappedBy = "person", cascade = CascadeType.ALL)
    private Patient patient;


    @Column(name = "email", nullable = false, unique = true, length = 200)
    private String email;

    @Column(name = "password", nullable = false, length = 20)
    private String password;

    @Column(name = "role")
    private String role;


    public Person() {
    }

    public Person(String email, String password, String role) {
//        this.id = id;
        this.email = email;
        this.password = password;
        this.role = role;
    }

    public Person(Integer id, String email, String password, String role) {
        this.id = id;
        this.email = email;
        this.password = password;
        this.role = role;
    }

    public Person(Integer id, String email) {
        this.id = id;
        this.email = email;

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
