package com.example.springdemo.entities;

import javax.persistence.*;

import java.sql.Date;
import java.util.List;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "medication_plan")
public class MedicationPlan {

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }



    public void setIntake_intervals(List<Intake_intervals> intake_intervals) {
        this.intake_intervals = intake_intervals;
    }

    public Date getStart_date() {
        return start_date;
    }

    public void setStart_date(Date start_date) {
        this.start_date = start_date;
    }

    public Date getEnd_date() {
        return end_date;
    }

    public void setEnd_date(Date end_date) {
        this.end_date = end_date;
    }

    public List<Medication> getMedications() {
        return medications;
    }

    public void setMedications(List<Medication> medications) {
        this.medications = medications;
    }

    @ManyToOne
    @JoinColumn(name = "patient_id", nullable = false)
    private Patient patient;

    @OneToMany(mappedBy = "medication_plan" , cascade = {
        CascadeType.ALL
    }, fetch = FetchType.LAZY)
    private List<Intake_intervals> intake_intervals;



    @Column(name = "start_date")
    private Date start_date;

    @Column(name = "end_date")
    private Date end_date;

    @ManyToMany(cascade = {
            CascadeType.ALL
    })
    @JoinTable(
            name = "medications_of_plan",
            joinColumns = @JoinColumn(name = "id"),
            inverseJoinColumns = @JoinColumn(name = "med_id"))
    private List<Medication> medications;

    public MedicationPlan(){}
    public MedicationPlan(Integer id, Date start_date, Date end_date){
        this.id = id;
        this.start_date=start_date;
        this.end_date=end_date;
    }
    public List<Intake_intervals> getIntake_intervals() {
        return intake_intervals;
    }
}
