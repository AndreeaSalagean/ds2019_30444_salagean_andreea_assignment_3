package com.example.springdemo.services;

import com.example.springdemo.dto.CPerson;
import com.example.springdemo.dto.CaregiverDTO;
import com.example.springdemo.dto.PPerson;
import com.example.springdemo.dto.PatientDTO;
import com.example.springdemo.dto.builders.CaregiverBuilder;
import com.example.springdemo.dto.builders.PatientBuilder;
import com.example.springdemo.entities.Caregiver;
import com.example.springdemo.entities.Patient;
import com.example.springdemo.entities.Person;
import com.example.springdemo.errorhandler.ResourceNotFoundException;
import com.example.springdemo.repositories.CaregiverRepository;
import com.example.springdemo.repositories.PatientRepository;
import com.example.springdemo.repositories.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class CaregiverService {

    private final CaregiverRepository caregiverRepository;
    private final PersonRepository personRepository;
    private final PatientRepository patientRepository;

    @Autowired
    public CaregiverService(CaregiverRepository caregiverRepository, PersonRepository personRepository, PatientRepository patientRepository) {
        this.caregiverRepository = caregiverRepository;
        this.personRepository = personRepository;
        this.patientRepository = patientRepository;
    }

    public CaregiverDTO findUserById(Integer id){
        Optional<Caregiver> person  = caregiverRepository.findById(id);

        if (!person.isPresent()) {
            throw new ResourceNotFoundException("Caregiver", "user id", id);
        }
        return CaregiverBuilder.generateDTOFromEntity(person.get());
    }

    public List<CaregiverDTO> getAll(){
        List<Caregiver> caregivers = caregiverRepository.findAll();
        return caregivers.stream()
                .map(CaregiverBuilder::generateDTOFromEntity)
                .collect(Collectors.toList());
    }

    public List<PatientDTO> getAvailablePatients(Integer id){
        Optional<Caregiver> caregiver  = caregiverRepository.findById(id);
        if (!caregiver.isPresent()) {
            throw new ResourceNotFoundException("Caregiver", "user id", id);
        }

        List<Patient> patients = caregiver.get().getPatients();
        List<Patient> allPat = patientRepository.findAll();

        allPat.removeAll(patients);
        return allPat.stream()
                .map(PatientBuilder::generateDTOFromEntity)
                .collect(Collectors.toList());

    }

    public Integer insert(CPerson caregiverDTO) {

        //PersonFieldValidator.validateInsertOrUpdate(personDTO);

        Caregiver caregiver = new Caregiver(null, caregiverDTO.getName(), caregiverDTO.getBirthDate(), caregiverDTO.getGender(), caregiverDTO.getAddress());
        Person personToAdd = new Person(null, caregiverDTO.getEmail(), caregiverDTO.getPassword(), "caregiver");
        caregiver.setPerson(personToAdd);

        return caregiverRepository
                .save(caregiver)
                .getId();
    }

    public Integer update(CaregiverDTO caregiverDTO) {

        Optional<Caregiver> person = caregiverRepository.findById(caregiverDTO.getId());

        if(!person.isPresent()){
            throw new ResourceNotFoundException("Caregiver", "id", caregiverDTO.getId().toString());
        }
        //PersonFieldValidator.validateInsertOrUpdate(personDTO);

        return caregiverRepository.save(CaregiverBuilder.generateEntityFromDTO(caregiverDTO)).getId();
    }

    public void delete(Integer id){
        this.personRepository.deleteById(id);
    }

    public List<PatientDTO> getPatientList(Integer id) {
        Optional<Caregiver> caregiver  = caregiverRepository.findById(id);
        if (!caregiver.isPresent()) {
            throw new ResourceNotFoundException("Caregiver", "user id", id);
        }
        List<Patient> patients = caregiver.get().getPatients();

        return patients.stream()
                .map(PatientBuilder::generateDTOFromEntity)
                .collect(Collectors.toList());

    }

    public void insertPatient(PatientDTO patient, Integer id) {
        Optional<Caregiver> caregiver  = caregiverRepository.findById(id);
        if (!caregiver.isPresent()) {
            throw new ResourceNotFoundException("Caregiver", "user id", id);
        }

        Caregiver car = caregiver.get();
        List<Patient> patients = car.getPatients();
        patients.add(PatientBuilder.generateEntityFromDTO(patient));
        car.setPatients(patients);
        caregiverRepository.save(car);
    }
    public void deletePatient(Integer id, Integer patientID){
        Optional<Caregiver> caregiver  = caregiverRepository.findById(id);

        Caregiver caregiver1 = caregiver.get();
        List<Patient> patients = caregiver1.getPatients();
        List<Patient> patientsToAdd = new ArrayList<>();
        for(Patient p : patients) {
            if(p.getId() != patientID) {
                patientsToAdd.add(p);
            }
        }
        caregiver1.setPatients(patientsToAdd);
        caregiverRepository.save(caregiver1);


    }
}
