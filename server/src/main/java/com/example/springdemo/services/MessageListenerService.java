package com.example.springdemo.services;

import com.example.springdemo.entities.Message;
import com.example.springdemo.entities.Patient;
import com.example.springdemo.repositories.MessageRepository;
import com.example.springdemo.repositories.PatientRepository;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DeliverCallback;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;


@Service
public class MessageListenerService {

    private final static String QUEUE_NAME = "hello";
    private final MessageRepository messageRepository;
    private final PatientRepository patientRepository;
    private final SimpMessagingTemplate simpleMessage;

    @Autowired
    public MessageListenerService(MessageRepository messageRepository, PatientRepository patientRepository, SimpMessagingTemplate simpleMessage) {
        this.messageRepository = messageRepository;
        this.patientRepository = patientRepository;
        this.simpleMessage = simpleMessage;
        try {
            readMessages();
        }catch(Exception e){
            System.out.println(e);
        }

    }
    @RabbitListener
    public void readMessages()throws Exception{
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        Connection connection = factory.newConnection();
        Channel channel = (Channel) connection.createChannel();

        channel.queueDeclare(QUEUE_NAME, false, false, false, null);
        System.out.println(" [*] Waiting for messages. To exit press CTRL+C");

        DeliverCallback deliverCallback = (consumerTag, delivery) -> {
            String message = new String(delivery.getBody(), "UTF-8");
            try {
                insert(processMessage(message));
            } catch (Exception e) {
                e.printStackTrace();
            }
            System.out.println(" [x] Received '" + message + "'");
        };
        channel.basicConsume(QUEUE_NAME, true, deliverCallback, consumerTag -> { });


    }
    public Message processMessage (String message) throws Exception{
        Object obj = new JSONParser().parse(message);
        JSONObject jo = (JSONObject) obj;

        String patientID = (String) jo.get("patientId").toString();
        String startTime = (String) jo.get("startTime");
        String endTime = (String) jo.get("endTime");
        String activity = (String) jo.get("activity");
         activity = activity.replace("\t", "");

        int id = Integer.parseInt(patientID);
        Date start_time = stringToDate(startTime);
        Date end_time = stringToDate(endTime);

        Optional<Patient> patient = patientRepository.findById(id);

        Patient pat = patient.get();

        Message mess = new Message(null,start_time, end_time, activity);
        mess.setPatient_id(pat);


        findAnomaly(mess);

        return mess;

    }

    public  long getActivityTime(Date start, Date finish) {

        return (finish.getTime() - start.getTime());

    }

    public void findAnomaly(Message s){
        switch (s.getActivity()){
            case "Sleeping":{
                Date start = s.getStart_time();
                Date finish = s.getEnd_time();

                if(TimeUnit.MILLISECONDS
                        .toSeconds(getActivityTime(start,finish))  > 60*60*12){
                    System.out.println("abnormality found in sleep period for patient:" + s.getPatient_id());
                    String str = "!! Abnormality found in sleeping at patient with id: " + s.getPatient_id().getId();
                    simpleMessage.convertAndSend("/topic/message",str );
                }
                break;
            }
            case "Leaving":{
                Date start = s.getStart_time();
                Date finish = s.getEnd_time();
                if(TimeUnit.MILLISECONDS
                        .toSeconds(getActivityTime(start,finish))  > 60*60*12){
                    System.out.println("abnormality found in leaving period for patient:" + s.getPatient_id());
                    String str = "!! Abnormality found in leaving activity at patient with id: " + s.getPatient_id().getId();
                    simpleMessage.convertAndSend("/topic/message",str );
                }
                break;
            }
            case "Toileting":{

                Date start = s.getStart_time();
                Date finish = s.getEnd_time();
                if(TimeUnit.MILLISECONDS
                        .toSeconds(getActivityTime(start,finish))  > 60*60){
                    System.out.println("abnormality found in toileting period for patient:" + s.getPatient_id());
                    String str = "!! Abnormality found in toileting at patient with id: " + s.getPatient_id().getId();
                    simpleMessage.convertAndSend("/topic/message",str );
                }
                break;
            }
            case "Grooming":{


                Date start = s.getStart_time();
                Date finish = s.getEnd_time();
                if(TimeUnit.MILLISECONDS
                        .toSeconds(getActivityTime(start,finish))  > 60*60){
                    //System.out.println("\nAnomaly found in grooming period for patient:\n" + s.getPatient_id() );

                    String str = " !! Abnormality found in grooming at patient with id: " + s.getPatient_id().getId();
                    simpleMessage.convertAndSend("/topic/message",str );;
                }
                break;
            }
            case "Showering":{
                Date start = s.getStart_time();
                Date finish = s.getEnd_time();
                if(TimeUnit.MILLISECONDS
                        .toSeconds(getActivityTime(start,finish))  > 60*60){
                    System.out.println("Anomaly found in showering period for patient:" + s.getPatient_id());
                    String str = "!! Abnormality found in showering activity at patient with id: " + s.getPatient_id().getId();
                    simpleMessage.convertAndSend("/topic/message",str );
                }
                break;
            }
        }



    }
    public Date stringToDate(String s) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try
        {
            Date date = simpleDateFormat.parse(s);
            return date;
        }
        catch (ParseException ex)
        {
            System.out.println("Exception "+ex);
        }
        return null;

    }
    public void insert(Message message) throws Exception{

        messageRepository.save(message);
    }
}
