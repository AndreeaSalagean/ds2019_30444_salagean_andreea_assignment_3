package com.example.springdemo.services;

import com.example.springdemo.entities.Intake_intervals;
import com.example.springdemo.repositories.IntakeIntevalsRepository;
import com.grpc.helloworld.MedNameTaken;
import com.grpc.helloworld.MedTakenGrpc;

import com.grpc.helloworld.ServerResponseTaken;
import io.grpc.stub.StreamObserver;
import org.lognet.springboot.grpc.GRpcService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@GRpcService
public class MedicationTakenImpl extends MedTakenGrpc.MedTakenImplBase {
    private static final Logger LOGGER =
            LoggerFactory.getLogger(MedicationTakenImpl.class);

    private final IntakeIntevalsRepository intakeIntevalsRepository;
    @Autowired
    public MedicationTakenImpl(IntakeIntevalsRepository intakeIntevalsRepository){
        this.intakeIntevalsRepository = intakeIntevalsRepository;
    }
    @Override
    public void setTaken(MedNameTaken request,
                         StreamObserver<ServerResponseTaken> responseObserver){

        LOGGER.info("server received {}", request);

        String[] requests = request.getName().split(" ");
        Integer id = Integer.parseInt(request.getMedId());
        List<Intake_intervals> intervals = intakeIntevalsRepository.findAll() ;
        if(requests[0].equals("Taken")){

            for(Intake_intervals i : intervals){
                if(i.getMedication().getId() == id){
                    i.setTaken(true);
                    intakeIntevalsRepository.save(i);
                }
            }
        }
        else{
            for(Intake_intervals i : intervals){
                if(i.getMedication().getId() == id){
                    i.setTaken(false);
                    intakeIntevalsRepository.save(i);
                }
            }
        }

        ServerResponseTaken response = ServerResponseTaken.newBuilder()
                .setRespTaken("Thank you for response" ).build();

        LOGGER.info("server responded {}", response);
        responseObserver.onNext(response);
        responseObserver.onCompleted();

    }}
