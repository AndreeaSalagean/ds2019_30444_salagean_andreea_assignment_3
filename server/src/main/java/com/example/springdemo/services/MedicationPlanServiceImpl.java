package com.example.springdemo.services;

import com.example.springdemo.entities.Intake_intervals;
import com.example.springdemo.entities.MedicationPlan;
import com.example.springdemo.repositories.IntakeIntevalsRepository;
import com.example.springdemo.repositories.MedicationPlanRepository;
import com.grpc.helloworld.*;
import io.grpc.stub.StreamObserver;
import org.lognet.springboot.grpc.GRpcService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@GRpcService
public class MedicationPlanServiceImpl extends MedPlanSErviceGrpc.MedPlanSErviceImplBase{

    private static final Logger LOGGER =
            LoggerFactory.getLogger(MedicationPlanServiceImpl.class);

    private final MedicationPlanRepository medicationPlanRepository;
    private final IntakeIntevalsRepository intakeIntevalsRepository;

    @Autowired
    public MedicationPlanServiceImpl(MedicationPlanRepository medicationPlanRepository, IntakeIntevalsRepository intakeIntevalsRepository){
        this.medicationPlanRepository = medicationPlanRepository;
        this.intakeIntevalsRepository = intakeIntevalsRepository;
    }



    public void getMedication(MedQuery request,
                              StreamObserver<MedicationResponse> responseObserver){

        LOGGER.info("server received {}", request);


        String message = request.getId();
        Optional<MedicationPlan> medicationPlan = medicationPlanRepository.findById(Integer.parseInt(message));

//        List<Intake_intervals> intake_intervalsList = medicationPlan.get().getIntake_intervals();
        List<Intake_intervals> intake_intervalsList = intakeIntevalsRepository.findAll();
        MedicationResponse.Builder response = MedicationResponse.newBuilder();
        for(Intake_intervals interval : intake_intervalsList){
            Medication medication = Medication.newBuilder().
                    setId(interval.getMedication().getId().toString())
                    .setName(interval.getMedication().getName())
                    .setDosage(interval.getMedication().getDosage())
                    .setStart(interval.getStart())
                    .setEnd(interval.getEnd())
                    .build();
            response.addMedications(medication);

        }


        LOGGER.info("server responded {}", response);
        responseObserver.onNext(response.build());
        responseObserver.onCompleted();

    }

}
