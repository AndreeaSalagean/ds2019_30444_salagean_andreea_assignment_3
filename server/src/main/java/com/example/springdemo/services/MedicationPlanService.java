package com.example.springdemo.services;


import com.example.springdemo.entities.Intake_intervals;
import com.example.springdemo.entities.Medication;
import com.example.springdemo.entities.MedicationPlan;
import com.example.springdemo.entities.Patient;
import com.example.springdemo.repositories.IntakeIntevalsRepository;
import com.example.springdemo.repositories.MedicationPlanRepository;
import com.example.springdemo.repositories.MedicationRepository;
import com.example.springdemo.repositories.PatientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class MedicationPlanService {


    private final PatientRepository patientRepository;
    private final MedicationRepository medicationRepository;
    private  final MedicationPlanRepository medicationPlanRepository;
    private final IntakeIntevalsRepository intakeIntevalsRepository;
    @Autowired
    public MedicationPlanService(PatientRepository patientRepository, MedicationRepository medicationRepository, MedicationPlanRepository medicationPlanRepository, IntakeIntevalsRepository intakeIntevalsRepository){this.patientRepository = patientRepository;
    this.medicationRepository = medicationRepository;
    this.medicationPlanRepository = medicationPlanRepository;
    this.intakeIntevalsRepository = intakeIntevalsRepository;
    //addMedicationPlan(1);

    }

    public void addMedicationPlan(Integer patientId){

        Optional<Patient> person  = patientRepository.findById(patientId);

        Patient patient = person.get();
        Date d  = new Date(2019-02-02);
        Date e = new Date(2019-02-12);


        MedicationPlan medicationPlan1 = new MedicationPlan(null, d,e);

        medicationPlan1.setPatient(patient);
        medicationPlanRepository.save(medicationPlan1);


        Medication med1 = new Medication(null, "nurofen", "none", "10 mg");
        medicationRepository.save(med1);

        Medication med2 = new Medication(null, "bioflu", "none", "150 mg");
        medicationRepository.save(med2);

        Medication med3 = new Medication(null, "triferment", "none", "20 mg");
        medicationRepository.save(med3);


        Intake_intervals intake1 = new Intake_intervals(null, "2019-02-11 10:30:00", "2019-02-11 11:30:00");
        Intake_intervals intake2 = new Intake_intervals(null, "2019-02-11 10:00:00", "2019-02-11 11:00:00");
        Intake_intervals intake3 = new Intake_intervals(null, "2019-02-11 13:00:00", "2019-02-11 13:30:00");

//        Intake_intervals intake11 = new Intake_intervals(null, "2019-02-11 20:00:00", "2019-02-11 21:00:00");
//        Intake_intervals intake21 = new Intake_intervals(null, "2019-02-11 17:00:00", "2019-02-11 18:00:00");

        intake1.setMedication_plan(medicationPlan1);
        intake1.setMedication(med1);
       // intake11.setMedication(med1);
       // intake11.setMedication_plan(medicationPlan1);

        intake2.setMedication(med2);
        intake2.setMedication_plan(medicationPlan1);
       // intake21.setMedication_plan(medicationPlan1);
       // intake21.setMedication(med2);

        intake3.setMedication_plan(medicationPlan1);
        intake3.setMedication(med3);

        intakeIntevalsRepository.save(intake1);
        intakeIntevalsRepository.save(intake2);
        intakeIntevalsRepository.save(intake3);
//        intakeIntevalsRepository.save(intake11);
//        intakeIntevalsRepository.save(intake21);

        List<Intake_intervals> listMed1 = new ArrayList<>();
        listMed1.add(intake1);



        List<Intake_intervals> listMed2 = new ArrayList<>();
        listMed2.add(intake2);


        List<Intake_intervals> listMed3 = new ArrayList<>();
        listMed3.add(intake3);



    }
}
