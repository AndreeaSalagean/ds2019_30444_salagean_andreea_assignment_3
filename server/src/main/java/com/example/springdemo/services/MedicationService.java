package com.example.springdemo.services;

import com.example.springdemo.dto.MedicationDTO;
import com.example.springdemo.dto.builders.MedicationBuilder;
import com.example.springdemo.entities.Medication;
import com.example.springdemo.errorhandler.ResourceNotFoundException;
import com.example.springdemo.repositories.MedicationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class MedicationService {

    private final MedicationRepository medicationRepository;

    @Autowired
    public MedicationService(MedicationRepository medicationRepository){this.medicationRepository = medicationRepository;}

    public MedicationDTO findUserById(Integer id){
        Optional<Medication> med  = medicationRepository.findById(id);

        if (!med.isPresent()) {
            throw new ResourceNotFoundException("Medication", "med id", id);
        }
        return MedicationBuilder.generateDTOFromEntity(med.get());
    }

    public List<MedicationDTO> getAll(){
        List<Medication> medications = medicationRepository.findAll();
        return medications.stream()
                .map(MedicationBuilder::generateDTOFromEntity)
                .collect(Collectors.toList());
    }

    public Integer insert(MedicationDTO medicationDTO) {

        //PersonFieldValidator.validateInsertOrUpdate(personDTO);

        return medicationRepository
                .save(MedicationBuilder.generateEntityFromDTO(medicationDTO))
                .getId();
    }

    public Integer update(MedicationDTO medDTO) {

        Optional<Medication> med = medicationRepository.findById(medDTO.getId());

        if(!med.isPresent()){
            throw new ResourceNotFoundException("Medication", "id", med.get().toString());
        }
        //PersonFieldValidator.validateInsertOrUpdate(personDTO);

        return medicationRepository.save(MedicationBuilder.generateEntityFromDTO(medDTO)).getId();
    }

    public void delete(Integer id){
        this.medicationRepository.deleteById(id);
    }
}
