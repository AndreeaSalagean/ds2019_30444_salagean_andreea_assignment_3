package com.example.springdemo.repositories;

import com.example.springdemo.entities.MedicationPlan;
import org.springframework.data.jpa.repository.JpaRepository;


public interface MedicationPlanRepository extends JpaRepository<MedicationPlan, Integer> {
}
