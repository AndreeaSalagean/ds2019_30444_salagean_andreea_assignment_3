package com.example.springdemo.repositories;

import com.example.springdemo.entities.Intake_intervals;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IntakeIntevalsRepository extends JpaRepository<Intake_intervals, Integer> {
}
